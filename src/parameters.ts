export interface Parameters {
  youtubeChannelId: string
  channelHashtag: string
  youtubeSecret: string
  twitterSecret: string
}

export const getParameters = (): Parameters => ({
  youtubeChannelId: process.env.YouTubeChannelId!,
  channelHashtag: process.env.ChannelHashtag!,
  youtubeSecret: process.env.YouTubeSecret!,
  twitterSecret: process.env.TwitterSecret!,
})

// for running locally
export const setParameters = (params: Parameters) => {
  process.env.YouTubeChannelId = params.youtubeChannelId
  process.env.ChannelHashtag = params.channelHashtag
  process.env.YouTubeSecret = params.youtubeSecret
  process.env.TwitterSecret = params.twitterSecret
}
