import AWS from 'aws-sdk'
import { Video } from './youtube/youtube'

const TableName = 'YouTubeToTwitterTable'

const ddb = new AWS.DynamoDB.DocumentClient({
  apiVersion: '2012-08-10',
  service: new AWS.DynamoDB({ region: process.env.AWS_REGION || 'us-east-1' }),
})

export const checkExistingVideoTweets = async (
  videos: Video[]
): Promise<Array<{ video: Video; alreadyTweeted: boolean }>> => {
  console.log(`Checking DynamoDB for videos: ${videos.map((_) => _.id).join(', ')}`)
  const response = await ddb
    .transactGet({
      TransactItems: videos.map((_) => ({
        Get: {
          TableName,
          Key: {
            video: _.id,
          },
        },
      })),
    })
    .promise()
  return response.Responses!.map((_, index) => ({
    video: videos[index],
    alreadyTweeted: !!_.Item,
  }))
}

export const markVideosAsProcessed = async (videos: Video[]) => {
  console.log(
    `Marking videos as processed to avoid duplicate tweets: ${videos.map((_) => _.id).join(', ')}`
  )
  const processed = new Date().toISOString()
  return ddb
    .transactWrite({
      TransactItems: videos.map((_) => ({
        Put: {
          TableName,
          Item: {
            video: _.id,
            processed,
          },
        },
      })),
    })
    .promise()
}
