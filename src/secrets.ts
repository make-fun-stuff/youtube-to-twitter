import AWS from 'aws-sdk'
import { getParameters } from './parameters'

interface YouTubeAuthSecret {
  apiKey: string
}

interface TwitterAuthSecret {
  consumerKey: string
  consumerSecret: string
  accessTokenKey: string
  accessTokenSecret: string
}

const secretsManager = new AWS.SecretsManager()

let youtubeSecret: YouTubeAuthSecret | undefined
let twitterSecret: TwitterAuthSecret | undefined

export const getYouTubeSecret = async (): Promise<YouTubeAuthSecret> => {
  if (!youtubeSecret) {
    const parameters = getParameters()
    const secret = await secretsManager
      .getSecretValue({
        SecretId: parameters.youtubeSecret,
      })
      .promise()
    youtubeSecret = JSON.parse(secret.SecretString!)
  }
  return youtubeSecret!
}

export const getTwitterSecret = async (): Promise<TwitterAuthSecret> => {
  if (!twitterSecret) {
    const parameters = getParameters()
    const secret = await secretsManager
      .getSecretValue({
        SecretId: parameters.twitterSecret,
      })
      .promise()
    twitterSecret = JSON.parse(secret.SecretString!)
  }
  return twitterSecret!
}
