import { getParameters } from '../parameters'
import { getClient } from './client'

export interface Video {
  id: string
  title: string
  description: string
  publishTime: Date
  url: string
}

// consumes 100 + maxResults YouTube API qutoa units
export const getMostRecentVideos = async (maxResults: number): Promise<Video[]> => {
  const client = await getClient()
  // search consumes 100 quota units
  const listResponse = await client.search.list({
    part: 'snippet',
    channelId: getParameters().youtubeChannelId,
    type: 'video',
    order: 'date',
    maxResults,
  })
  if (listResponse.status !== 200) {
    throw Error(
      `Received unexpected repsonse from YouTube search query: ${listResponse.status}, ${listResponse.statusText}`
    )
  }
  console.log(
    `Received ${listResponse.data.items.length} videos for channel ${
      getParameters().youtubeChannelId
    }`
  )
  // consumes "maxResults" quota unit(s)
  return Promise.all(
    listResponse.data.items.map(async (_: any) => {
      const detailsResponse = await client.videos.list({
        part: 'snippet',
        id: _.id.videoId,
      })
      if (detailsResponse.status !== 200) {
        throw Error(
          `Received unexpected repsonse from YouTube details query: ${detailsResponse.status}, ${detailsResponse.statusText}`
        )
      }
      const details = detailsResponse.data.items[0].snippet
      return {
        id: _.id.videoId,
        title: details.title,
        description: details.description,
        publishTime: new Date(details.publishedAt),
        url: `https://youtu.be/${_.id.videoId}`,
      }
    })
  )
}
