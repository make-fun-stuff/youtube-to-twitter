import { getYouTubeSecret } from '../secrets'

let client: any = undefined

const youtube = require('@googleapis/youtube')

export const getClient = async () => {
  if (!client) {
    const auth = await getYouTubeSecret()
    client = youtube.youtube({
      version: 'v3',
      auth: auth.apiKey,
    })
  }
  return client
}
