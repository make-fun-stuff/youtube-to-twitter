import { checkExistingVideoTweets, markVideosAsProcessed } from './dynamodb'
import { tweet } from './twitter/twitter'
import { getMostRecentVideos } from './youtube/youtube'

export const handler = async (_event?: any, _context?: any) => {
  const videos = await getMostRecentVideos(1)
  const withAlreadyProcessedCheck = await checkExistingVideoTweets(videos)
  const newVideos = withAlreadyProcessedCheck.filter((_) => !_.alreadyTweeted)
  if (!newVideos.length) {
    console.log('All most recent videos have already been processed')
    return
  }

  // do this before tweeting so if there's a bug/error here doesn't result in multiple tweets
  await markVideosAsProcessed(newVideos.map((_) => _.video))

  await Promise.all(newVideos.map((_) => tweet(_.video)))
}
