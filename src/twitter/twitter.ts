import { getParameters } from '../parameters'
import { Video } from '../youtube/youtube'
import { getCharacterCount, getCharacterLimit, getClient } from './client'

// update this with keywords that you want converted to hashtags
const hashtagableKeywords: Record<string, string> = {
  arduino: 'Arduino',
  'raspberry pi': 'RaspberryPi',
  diy: 'DIY',
  maker: 'Maker',
  'april fools': 'AprilFools',
  gnome: 'Gnome',
  tutorial: 'Tutorial',
  christmas: 'Christmas',
  adafruit: 'Adafruit',
  aws: 'AWS',
  'star wars': 'StarWars',
  'the mandalorian': 'TheMandalorian',
  'this is the way': 'ThisIsTheWay',
  'baby yoda': 'BabyYoda',
  pokemon: 'Pokemon',
  'home automation': 'HomeAutomation',
}

const hashtagsToIgnore = ['#Shorts']

const trimLeadingSpaces = (content: string) => {
  return content
    .split('\n')
    .map((_) => _.trim())
    .join('\n')
    .trim()
}

const withHashtags = (content: string) => {
  Object.keys(hashtagableKeywords).forEach((keyword) => {
    const lower = content.toLowerCase()
    const match = lower.match(new RegExp(`(^|\\s)(${keyword})($|\\s|\\.|,'";:\\?\\!\\))`))
    const index = match
      ? match[0].match('\\s')
        ? lower.indexOf(match[0]) + 1
        : lower.indexOf(match[0])
      : -1
    if (index >= 0) {
      content = `${content.slice(0, index)}#${hashtagableKeywords[keyword]}${content.slice(
        index + keyword.length,
        content.length
      )}`
    }
  })
  return content
}

const findHashtags = (content: string): string[] => {
  return content.match(new RegExp('#[a-zA-Z]+', 'g')) || []
}

const composeTweet = (video: Video): string => {
  const shortDescription = video.description.split('\n\n')[0]
  const description = withHashtags(shortDescription)
  const channelHashtag = getParameters().channelHashtag
  const videoHashtags = findHashtags(video.description).filter(
    (hashtag) =>
      !description.includes(hashtag) &&
      hashtag !== channelHashtag &&
      !hashtagsToIgnore.includes(hashtag)
  )
  const tweet = trimLeadingSpaces(`
        ${video.title.split('|')[0]}

        ${description}

        ${channelHashtag} ${videoHashtags.join(' ')}

        ${video.url}
    `)

  const characterCount = getCharacterCount(tweet)
  console.log(
    `Tweet content for video ${video.id} (${characterCount} characters):\n------\n${tweet}\n------`
  )
  if (characterCount >= getCharacterLimit()) {
    throw Error(`Tweet too long! (${characterCount} characters)`)
  }
  return tweet
}

export const tweet = async (video: Video): Promise<void> => {
  const client = await getClient()
  const twitterResponse = await new Promise((resolve, reject) => {
    client.post(
      'statuses/update',
      { status: composeTweet(video) },
      (error: any, _tweet: any, response: any) => {
        if (error) {
          reject(error)
        }
        resolve(response)
      }
    )
  })
  console.log(`Twitter API response: ${JSON.stringify(twitterResponse, null, 2)}`)
}
