import { getTwitterSecret } from '../secrets'

let client: any = undefined

const Twitter = require('twitter')

export const getClient = async () => {
  if (!client) {
    const auth = await getTwitterSecret()
    client = new Twitter({
      consumer_key: auth.consumerKey,
      consumer_secret: auth.consumerSecret,
      access_token_key: auth.accessTokenKey,
      access_token_secret: auth.accessTokenSecret,
    })
  }
  return client
}

export const getCharacterLimit = () => 280

export const getCharacterCount = (content: string) => content.length
