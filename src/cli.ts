import { handler } from '.'
import { setParameters } from './parameters'
const yargs = require('yargs')

const args = yargs
  .option('ytChannel', {
    describe: 'Your YouTube channel id',
    required: true,
    type: 'string',
  })
  .option('ht', {
    describe: 'Your channel hashtag, which will be included in every tweet',
    required: true,
    type: 'string',
  })
  .option('ytSecret', {
    describe: 'The ARN of a SecretsManager secret containing your YouTube API key',
    required: true,
    type: 'string',
  })
  .option('twitterSecret', {
    describe: 'The ARN of a SecretsManager secret containing your Twitter auth information',
    required: true,
    type: 'string',
  })
  .strict().argv

setParameters({
  youtubeChannelId: args.ytChannel,
  channelHashtag: args.ht,
  youtubeSecret: args.ytSecret,
  twitterSecret: args.twitterSecret,
})
handler().catch(console.log)
